/*
Write a program that determines whether a meeting room is in violation of fire 
law regulations regarding the maximum room capacity. The program will read in 
the maximum room capacity and the number of people to attend the meeting. If the
number of people is less than or equal to the maximum room capacity, the program
announces that it is legal to hold the meeting and tells how many additional 
people may legally attend. If the number of people exceeds the maximum room c
apacity, the program announces that the meeting cannot be held as planned due to 
fire regulations and tells how many people must be excluded in order to meet the 
fire regulations.
*/

#include <iostream>
using namespace std;

int main()
{
    int max_capacity = 0;
    int n_people = 0;

    cout << "Enter meeting room capacity: ";
    cin >> max_capacity;

    cout << "Enter number of attendees: ";
    cin >> n_people;

    if (n_people <= max_capacity) {
        cout << "It is legal to hold the meeting\n";
        cout << "You can add " << max_capacity - n_people << " more people\n";
    }
    else {
        cout << "You cannot held a meeting due fire regulation rules\n";
        cout << "You need to remove " << n_people - max_capacity << " people\n";
    }

    return 0;
}