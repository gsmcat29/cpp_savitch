/*
It is difficult to make a budget that spans several years, because prices are 
not stable. If your company needs 200 pencils per year, you cannot simply use 
this year’s price as the cost of pencils two years from now. Because of 
inflation the cost is likely to be higher than it is today. Write a program to 
gauge the expected cost of an item in a specified number of years. 
The program asks for the cost of the item, the number of years from now that the 
item will be purchased, and the rate of inflation. The program then outputs the 
estimated cost of the item after the specified period. Have the user enter the 
inflation rate as a percentage, such as 5.6 (percent). Your program should then 
convert the percentage to a decimal fraction, such as 0.056, and should use a 
loop to estimate the price adjusted for inflation. 
(Hint: Use a loop.)
*/

#include <iostream>
using namespace std;

int main()
{
    int n_years = 0;
    double cost_item = 0.0;
    double inflation_rate = 0.0;
    double estimate_cost = 0.0;

    cout << "Enter item cost: ";
    cin >> cost_item;

    cout << "Enter number of years: ";
    cin >> n_years;

    cout << "Enter inflation rate: ";
    cin >> inflation_rate;

    inflation_rate = inflation_rate / 100.0;

    for (int i = 0; i < n_years; ++i) {
        estimate_cost = (cost_item * inflation_rate) + cost_item;
        cost_item = estimate_cost;
    }

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    cout << "Estimate cost after " << n_years << ": $" << estimate_cost << '\n';
    
    return 0;
}