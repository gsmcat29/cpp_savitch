/**
 * Workers at a particular company have won a 7.6% pay increase retroactive for 
 * six months. Write a program that takes an employee’s previous annual salary 
 * as input and outputs the amount of retroactive pay due the employee, the new 
 * annual salary, and the new monthly salary. Use a variable declaration with 
 * the modifier const to express the pay increase.
*/

#include <iostream>
using namespace std;

int main()
{   
    const double PAY_INCREASE = 0.076;

    double previous_salary = 0.0;        // annual
    double previous_monthly = 0.0;
    double new_monthly = 0.0;
    double retroactive_pay = 0.0;       // monthly salary
    double new_annual = 0.0;

    cout << "Enter your previous salary: ";
    cin >> previous_salary;

    previous_monthly = previous_salary / 12.0;
    new_monthly = previous_monthly + (previous_monthly * PAY_INCREASE);

    retroactive_pay = new_monthly - previous_monthly;
    new_annual = (retroactive_pay * 12) + (new_monthly * 12);

    cout << "Your retroactive pay is " << retroactive_pay << endl;
    cout << "Your new monthly pay is " << new_monthly << endl;
    cout << "Your new annual pay is " << new_annual << endl;

    return 0;
}