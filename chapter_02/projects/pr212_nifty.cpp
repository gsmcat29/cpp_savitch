/*
This problem is based on a “Nifty Assignment” by Steve Wolfman (http://nifty.
stanford.edu/2006/wolfman-pretid). Consider lists of numbers from real-life data
sources, for example, a list containing the number of students enrolled in 
different course sections, the number of comments posted for different Facebook 
status updates, the number of books in different library holdings, the number of 
votes per precinct, etc. It might seem like the leading digit of each number in 
the list should be 1–9 with an equally likely probability. However, Benford’s 
Law states that the leading digit is 1 about 30% of the time and drops with larger digits. The leading
digit is 9 only about 5% of the time.

Write a program that tests Benford’s Law. Collect a list of at least one hundred
numbers from some real-life data source and enter them into a text file. 
Your program should loop through the list of numbers and count how many times 1 
is the first digit, 2 is the first digit, etc. For each digit, output the 
percentage it appears as the first digit.

If you read a number into the string variable named strNum then you can access
the first digit as a char by using strNum[0]. This is described in more detail in
Chapter 9.
*/

#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    fstream inFile;
    inFile.open("livejournal.txt");

    char digit;
    string strNum;
    int countLines = 0;
    double ones = 0, twos = 0, threes = 0, fours = 0, fives = 0, 
           sixs = 0, sevens = 0, eights = 0, nines = 0;

    while(inFile >> strNum) {
        digit = strNum[0];
        //cout << digit << endl;

        ++countLines;

        switch(digit) {
            case '1':
                ++ones;     break;
            case '2':
                ++twos;     break;
            case '3':
                ++threes;   break;
            case '4':
                ++fours;    break;
            case '5':
                ++fives;    break;
            case '6':
                ++sixs;     break;
            case '7':
                ++sevens;   break;
            case '8':
                ++eights;   break;
            case '9':
                ++nines;    break;
        }
    }

    cout << "Total percentage for all the values is:\n";

    cout << "1 = " << (ones / countLines) * 100.0 << "%\n";
    cout << "2 = " << (twos/countLines) * 100 << "%\n";
    cout << "3 = " << (threes/countLines) * 100.0 << "%\n";
    cout << "4 = " << (fours/countLines) * 100.0 << "%\n";
    cout << "5 = " << (fives/countLines) * 100.0 << "%\n";
    cout << "6 = " << (sixs/countLines) * 100.0 << "%\n";
    cout << "7 = " << (sevens/countLines) * 100.0 << "%\n";
    cout << "8 = " << (eights/countLines) * 100.0 << "%\n";
    cout << "9 = " << (nines/countLines) * 100.0 << "%\n";

    return 0;
}