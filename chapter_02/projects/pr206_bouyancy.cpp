/*
Buoyancy is the ability of an object to float. Archimedes’ Principle states that 
the buoyant force is equal to the weight of the fluid that is displaced by the 
submerged object. The buoyant force can be computed by

F_b = V * gamma

where F_b is the buoyant force, V is the volume of the submerged object, and g 
is the specific weight of the fluid. If Fb is greater than or equal to the 
weight of the object, then it will float, otherwise it will sink.

 Write a program that inputs the weight (in pounds) and radius (in feet) of a 
 sphere and outputs whether the sphere will sink or float in water. 
 Use gamma = 62.4 lb/ft3 as the specific weight of water. 
 The volume of a sphere is computed by (4/3)pi*r3.
*/
#include <iostream>
using namespace std;

int main()
{
    const double PI = 3.141592;
    const double GAMMA = 62.4;

    double volume_sphere = 0.0;
    double f_b = 0.0;

    double radius = 0.0;
    double weight = 0.0;
    
    cout << "Enter weight (in pounds): ";
    cin >> weight;

    cout << "Enter radius (in feet): ";
    cin >> radius;

    // compute sphere volume
    volume_sphere = (4/3.0) * PI * (radius * radius * radius);

    f_b = volume_sphere * GAMMA;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    if (f_b >= weight) {
        cout << "Object will float\n";
    }
    else {
        cout << "Object will sink\n";
    }

    return 0;
}
