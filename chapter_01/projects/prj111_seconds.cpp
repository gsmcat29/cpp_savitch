/*
Write a program that inputs an integer that represents a length of time in 
seconds. The program should then output the number of hours, minutes, and 
seconds that corresponds to that number of seconds. For example, if the user 
inputs 50391 total seconds then the program should output 13 hours, 59 minutes, 
and 51 seconds.
*/

#include <iostream>
using namespace std;

int main()
{
    const int SECONDS_PER_HOUR = 3600;
    const int SECOND_PER_MINUTE = 60;

    int number_seconds = 0;

    cout << "Enter amount of seconds: ";
    cin >> number_seconds;

    double hour_seconds = 0;
    double mins_seconds = 0;
    int seconds = 0;
    int remainder = 0;
    int extra_seconds = 0;

    hour_seconds = number_seconds / SECONDS_PER_HOUR;
    remainder = number_seconds % SECONDS_PER_HOUR;

    mins_seconds =  remainder / SECOND_PER_MINUTE;
    extra_seconds = remainder;
    remainder =  extra_seconds % SECOND_PER_MINUTE;

    seconds = remainder;

    cout << hour_seconds << " hours " << mins_seconds << " minutes, and "
         << seconds << " seconds\n";
    
    return 0;
}