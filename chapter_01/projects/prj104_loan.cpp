/**
 * Negotiating a consumer loan is not always straightforward. One form of loan 
 * is the discount installment loan, which works as follows. Suppose a loan has 
 * a face value of $1,000, the interest rate is 15%, and the duration is 18 
 * months. The interest is computed by multiplying the face value of $1,000 by 
 * 0.15, yielding $150. That figure is then multiplied by the loan period of 1.5 
 * years to yield $225 as the total interest owed. That amount is immediately 
 * deducted from the face value, leaving the consumer with only $775. 
 * Repayment is made in equal monthly installments based on the face value. 
 * So the monthly loan payment will be $1,000 divided by 18, which is $55.56. 
 * This method of calculation may not be too bad if the consumer needs $775, but 
 * the calculation is a bit more complicated if the consumer needs $1,000. 
 * Write a program that will take three inputs: the amount the consumer needs
 * to receive, the interest rate, and the duration of the loan in months. 
 * The program should then calculate the face value required in order for the 
 * consumer to receive the amount needed. It should also calculate the monthly 
 * payment
*/

#include <iostream>
using namespace std;

int main()
{
    //const double INTEREST_RATE = 0.15;

    double duration = 0;
    double init_value = 0.0;
    double interest = 0.0;
    
    double monthly_payment = 0.0;
    double interest_rate = 0.0;
    double face_value = 0.0;

    cout << "Enter the amount you need: ";
    cin >> face_value;

    init_value = face_value;

    cout << "Enter interest rate: ";
    cin >> interest_rate;

    interest_rate /= 100.0;

    cout << "Enter duration in months: ";
    cin >> duration;

    duration = duration / 12.0;

    interest = (init_value * interest_rate) * duration;
    face_value = face_value - interest;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    cout << "Interest: " << interest << endl;
    cout << "Final amount " << face_value << endl;

    cout << "In case you need " << init_value << " as final ....\n";

    double desired_payment = 0;

    desired_payment = init_value / (1 - (duration * interest_rate));

    cout << "You face value should be: " << desired_payment << endl;

    monthly_payment = desired_payment / 12;

    cout << "Your monthly payment is: "  << monthly_payment << endl;
}