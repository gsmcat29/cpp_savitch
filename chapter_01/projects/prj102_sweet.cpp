/*
A government research lab has concluded that an artificial sweetener commonly
used in diet soda will cause death in laboratory mice. A friend of yours is 
desperate to lose weight but cannot give up soda. Your friend wants to know how 
much diet soda it is possible to drink without dying as a result. 
Write a program to supply the answer. The input to the program is the amount of 
artificial sweetener needed to kill a mouse, the weight of the mouse, and the 
weight of the dieter. To ensure the safety of your friend, be sure the program 
requests the weight at which the dieter will stop dieting, rather than the 
dieter’s current weight. Assume that diet soda contains one-tenth of 1% 
artificial sweetener. Use a variable declaration with the modifier const to give 
a name to this fraction. You may want to express the percentage as the double 
value 0.001.
*/

#include <iostream>
using namespace std;

int main()
{
    const double ARITFICIAL_SWEET = 0.001;

    double amount_sweet = 0.0;
    double weight_mouse = 0.0;
    double weight_friend = 0.0;     // weight where dieter will stop dieting

    cout << "Enter weight of mouse (in grams): ";
    cin >> weight_mouse;

    cout << "Enter weght of dieter (kilograms): ";
    cin >> weight_friend;

    cout << "Enter amount of artificial sweetener to kill mouse: ";
    cin >> amount_sweet;

    // given that we do not have a formula in the program to how much amount 
    // to kill a mouse, using google mentions around 150 ~ 200 mg

    double kill_mouse = 0.0;
    double kill_friend = 0.0;

    kill_mouse = (amount_sweet / weight_mouse) * weight_friend;
    kill_friend = ARITFICIAL_SWEET / kill_mouse;

    cout << "To kill your friend it would take " << kill_friend << " grams\n";

    return 0;
}