/*
One way to measure the amount of energy that is expended during exercise is to
use metabolic equivalents (MET). Here are some METS for various activities:

Running 6 MPH: 10 METS
Basketball: 8 METS
Sleeping: 1 MET

The number of calories burned per minute may be estimated using the formula

Calories/Minute = 0.0175 : 1 MET : (Weight in kilograms)

Write a program that inputs a subject’s weight in pounds, the number of METS 
for an activity, and the number of minutes spent on that activity, and then 
outputs an estimate for the total number of calories burned. 
One kilogram is equal to 2.2 pounds.
*/

#include <iostream>
using namespace std;

int main()
{
    const double POUND_KILO = 2.2;

    double weight_pounds = 0.0;
    int n_METS = 0.0;
    int number_minutes = 0;

    double total_calories = 0.0;
    double kilograms = 0.0;

    cout << "Enter your weight in pounds: ";
    cin >> weight_pounds;

    kilograms =   weight_pounds / POUND_KILO; 

    cout << "How many METS for activity: ";
    cin >> n_METS;

    cout << "Enter number of minutes: ";
    cin >> number_minutes;

    total_calories = 0.0175 * n_METS * kilograms;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    cout << "\nTotal number of calories burned: " << total_calories << endl;

    return 0;
}