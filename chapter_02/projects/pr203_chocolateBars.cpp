/*
Suppose you can buy a chocolate bar from the vending machine for $1 each. Inside
every chocolate bar is a coupon. You can redeem seven coupons for one chocolate
bar from the machine. You would like to know how many chocolate bars you can
eat, including those redeemed via coupon, if you have n dollars.

For example, if you have 20 dollars then you can initially buy 20 chocolate bars

This gives you 20 coupons. You can redeem 14 coupons for two additional 
chocolate bars. These two additional chocolate bars give you two more coupons, 
so you now have a total of eight coupons. This gives you enough to redeem for 
one final chocolate bar. As a result you now have 23 chocolate bars and two 
leftover coupons.

Write a program that inputs the number of dollars and outputs how many 
chocolate bars you can collect after spending all your money and redeeming as 
many coupons as possible. Also output the number of leftover coupons. 
The easiest way to solve this problem is to use a loop.
*/

#include <iostream>
using namespace std;

int main()
{
    const int COUPONS_FOR_CHOCOLATE = 7;

    int n_dollars = 0;
    int n_chocolates = 0;
    int leftover = 0;
    int coupons = 0;

    cout << "Enter number of dollars: ";
    cin >> n_dollars;

    while (n_dollars > 0) {
        ++n_chocolates;
        ++coupons;
        --n_dollars;
        if (coupons % COUPONS_FOR_CHOCOLATE == 0) {
            ++coupons;
            ++n_chocolates;
        }
    }

    leftover = coupons / COUPONS_FOR_CHOCOLATE;

    cout << "# of chocolate bars: " << n_chocolates << endl;
    cout << "# of leftover coupons: "  <<  leftover << endl;

    return 0;
}