/*
Create a text file that contains 10 integers with one integer per line. You can 
enter any 10 integers that you like in the file. Then write a program that 
inputs a number from the keyboard and determines if any pair of the 10 integers 
in the text file adds up to exactly the number typed in from the keyboard. 
If so, the program should output the pair of integers. If no pair of integers 
adds up to the number, then the program should output “No pair found.”
*/

// assuming a normal transverse of the value
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    fstream inFile;
    inFile.open("pair.txt");

    int number = 0;
    int p0 = 0;
    int p1 = 0;
    int p2 = 0;
    int p3 = 0;
    int p4 = 0;
    int p5 = 0;
    int p6 = 0;
    int p7 = 0;
    int p8 = 0;
    int p9 = 0;

    cout << "Please enter a number: ";
    cin >> number;

    inFile >> p0;
    inFile >> p1;
    inFile >> p2;
    inFile >> p3;
    inFile >> p4;
    inFile >> p5;
    inFile >> p6;
    inFile >> p7;
    inFile >> p8;
    inFile >> p9;

    inFile.close();

    int value = 0;

    // we need to compare every variable
    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p1) == number){
            cout << p1 << " " << value << endl;
            exit(0);
        }
        
    }
    inFile.close();

    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p2) == number){
            cout << p2 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();


    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p3) == number){
            cout << p3 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();



    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p4) == number){
            cout << p4 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();



    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p5) == number){
            cout << p5 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();



    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p6) == number){
            cout << p6 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();



    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p7) == number){
            cout << p7 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();



    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p8) == number){
            cout << p8 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();



    inFile.open("pair.txt");
    while(inFile >> value) {
        if ((value + p9) == number){
            cout << p9 << " " << value << endl;
            exit(0);
        }
    }
    inFile.close();

    cout << "No pair foud\n";


    return 0;
}


// in case of consecutive numbers
    /*while(inFile >> p1 >> p2) {
        ans = p1 + p2;

        if (ans == number) {
            cout << "Pair found: "  << p1 << ", " << p2 << endl;
            exit(0);
        }

    }

    cout << "No pair found\n";*/
