// magic formula for decimal point
#include <iostream>
using namespace std;

int main()
{
    const double value = 78.5078;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    cout << "The final value is " << value << endl;

    return 0;
}