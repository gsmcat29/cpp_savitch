// the or operator, ||
#include <iostream>
using namespace std;

int main()
{
    int x = 0;
    int y = 0;

    cout << "Type values for x and y: ";
    cin >> x >> y;

    if ((x == 1) || (x == y))
        cout << "x is 1 or x equals y\n";
    else
        cout << "x is neither 1 nor equal to y\n";
    
    return 0;
}