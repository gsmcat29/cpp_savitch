/**
 * A metric ton is 35,273.92 ounces. Write a program that will read the weight 
 * of a package of breakfast cereal in ounces and output the weight in metric 
 * tons as well as the number of boxes needed to yield one metric ton of cereal.
*/

#include <iostream>
using namespace std;

int main()
{
    const double METRIC_TON = 35273.92;
    double weight_ounces = 0;
    double weight_metric = 0;
    double number_boxes = 0;

    cout << "Enter weight in ounces of cereal box: ";
    cin >> weight_ounces;


    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(6);

    weight_metric = weight_ounces / METRIC_TON;

    cout << "Equivalent weight in metric tons is: " << weight_metric << endl;

    number_boxes = METRIC_TON / weight_ounces;
    
    cout.precision(2);
    cout << "Equivalent number of cereal boxes : " << number_boxes << endl;

    return 0;    
}