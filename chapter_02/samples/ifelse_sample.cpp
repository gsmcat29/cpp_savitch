//if-else statement
#include <iostream>
using namespace std;

int main()
{
    int myScore = 0;
    int yourScore = 0;
    int wager = 0;

    cout << "Enter myScore: ";
    cin >> myScore;

    cout << "Enter yourScore: ";
    cin >> yourScore;
    

    if (myScore > yourScore) {
        cout << "I win\n";
        wager = wager + 100;
    }
    else {
        cout << "I wish these were golf scores\n";
        wager = 0;
    }

    return 0;
}