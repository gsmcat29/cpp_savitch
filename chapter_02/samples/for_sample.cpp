// for statement
#include <iostream>
using namespace std;

int main()
{
    int number = 100;

    while (number >= 0) {
        cout << number << " bottles of beer on the shelf\n";
        number--;
    }

    return 0;
}