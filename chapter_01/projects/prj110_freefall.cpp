/*
Write a program that allows the user to enter a time in seconds and then outputs
how far an object would drop if it is in freefall for that length of time. 
Assume no friction or resistance from air and a constant acceleration of 32 feet 
per second due to gravity. Use the equation

Distance = 1/2 X acceleration X time^2

*/

#include <iostream>
using namespace std;

int main()
{
    const double ACCELERATION = 32; // 32 ft/sec^2

    int time = 0;
    double distance = 0;

    cout << "Enter time in seconds: ";
    cin >> time;

    distance = 0.5 * ACCELERATION * (time * time);

    cout << "Distance = " << distance << endl;

    return 0;
}