/*
Scientists estimate that consuming roughly 10 grams of caffeine at once is a 
lethal overdose. Write a program that inputs the number of milligrams of 
caffeine in a drink and outputs how many of those drinks it would take to kill a 
person. A 12-ounce can of cola has approximately 34 mg of caffeine, while a 
16-ounce cup of coffee has approximately 160 mg of caffeine.
*/

#include <iostream>
using namespace std;

int main()
{
    double milligrams = 0;
    double n_drinks = 0.0;

    cout << "Enter number of milligrams in a drink: ";
    cin >> milligrams;
    // 10 grams = 10,000 milligrams
    n_drinks = 10000 / milligrams;


    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    
    cout << "To kill you, you need to consume: " << n_drinks << " drinks\n";

    return 0;
}