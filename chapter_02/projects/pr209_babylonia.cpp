/*
(This is an extension of an exercise from Chapter 1.) The Babylonian algorithm 
to compute the square root of a positive number n is as follows:

1. Make a guess at the answer (you can pick n/2 as your initial guess).
2. Compute r = n / guess.
3. Set guess= (guess + r) / 2.
4. Go back to step 2 for as many iterations as necessary. The more steps 2 and 3
are repeated, the closer guess will become to the square root of n.

Write a program that inputs a double for n, iterates through the Babylonian 
algorithm until the guess is within 1% of the previous guess, and outputs the 
answer as a double to
*/

#include <iostream>
using namespace std;

int main()
{
    double n = 0;
    double r = 0.0;
    double guess = 0.0;
    double previous = 0.0; 

    cout << "Enter value of n: ";
    cin >> n; 

    guess = n /2.0;

    do {
        r = n / guess;
        previous = guess;
        guess = (guess + r) / 2.0;
        cout << "**guess: " << guess << endl;
    } while (guess > (1.01 * previous) || guess < (0.99 * previous));

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    cout << "The approximate square root of n is " << guess << endl;

    return 0;
}