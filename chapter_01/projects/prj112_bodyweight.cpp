/*
A simple rule to estimate your ideal body weight is to allow 110 pounds for the
first 5 feet of height and 5 pounds for each additional inch. Write a program 
with a variable for the height of a person in feet and another variable for the 
additional inches and input values for these variables from the keyboard. 
Assume the person is at least 5 feet tall. For example, a person that is 6 feet 
and 3 inches tall would be represented with a variable that stores the number 6 
and another variable that stores the number 3. Based on these values calculate 
and output the ideal body weight.
*/

#include <iostream>
using namespace std;

int main()
{   
    const int MIN_WEIGHT = 110;

    int feet = 0;
    int inch = 0;
    int diff = 0;
    double weight = 0.0;

    cout << "Assuming a height of 5 feet, enter new height:\n";
    cout << "Enter feet: ";
    cin >> feet;
    cout << "Enter inches: ";
    cin >> inch;

    if (feet > 5) {
        diff = (feet - 5) * 12;
        weight = MIN_WEIGHT + (diff * 5) + (inch * 5);
    }
    else {
        weight = MIN_WEIGHT + (inch * 5);
    }

    cout << "Ideal final weight is: " << weight << " pounds\n";

    return 0;
}