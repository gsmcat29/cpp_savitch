/*
(This is an extension of an exercise from Chapter 1.) A simple rule to estimate
your ideal body weight is to allow 110 pounds for the first 5 feet of height 
and 5 pounds for each additional inch. Create the following text in a text file. 
It contains the names and heights in feet and inches of Tom Atto (6'3"), Eaton Wright (5'5"),
and Cary Oki (5'11"):

Tom Atto
6
3
Eaton Wright
5
5
Cary Oki
5
11

Write a program that reads the data in the file and outputs the full name and 
ideal body weight for each person. Use a loop to read the names from the file. 
Your program should also handle an arbitrary number of entries in the file 
instead of handling only three entries.
*/

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{   
    const int MIN_WEIGHT = 110;

    int feet = 0;
    int inch = 0;
    int diff = 0;
    double weight = 0.0;

    string first;
    string last;
    
    fstream infile;
    cout << "Assuming a height of 5 feet, reading heights:\n";
    infile.open("body.txt");

    while (infile >> first >> last >> feet >> inch) {
        cout << "Full name: " << first << " " << last << endl;
        //cout << "Height: " << feet << "' " << inch << "''" << endl;
        if (feet > 5) {
            diff = (feet - 5) * 12;
            weight = MIN_WEIGHT + (diff * 5) + (inch * 5);
        }
        else {
            weight = MIN_WEIGHT + (inch * 5);
        }

        cout << "Ideal weight: " << weight << " pounds\n\n";
    }

    return 0;
}