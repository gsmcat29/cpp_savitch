/*
The video game machines at your local arcade output coupons depending on how
well you play the game. You can redeem 10 coupons for a candy bar or 3 coupons
for a gumball. You prefer candy bars to gumballs. Write a program that inputs the
number of coupons you win and outputs how many candy bars and gumballs you
can get if you spend all of your coupons on candy bars first and any remaining
coupons on gumballs.
*/

#include <iostream>
using namespace std;

int main()
{
    const int CANDYBAR_T = 10;
    const int GUMBALL_T = 3;

    int n_coupons = 0;
    int candybars = 0;
    int gumballs = 0;

    cout << "How many coupons ?: ";
    cin >> n_coupons;

    candybars = n_coupons / CANDYBAR_T;
        //cout << "If you use all your coupons in one items:\n";
    cout << "# of candybars: " << candybars << endl;

    cout << "Using remainder on gumballs:\n";

    int remainder = n_coupons - (candybars * CANDYBAR_T); 
    gumballs = remainder / GUMBALL_T;
    
    cout << "# of gumballs:  " << gumballs << endl;

    return 0;
}