/*
An employee is paid at a rate of $16.78 per hour for regular hours worked in a
week. Any hours over that are paid at the overtime rate of one and one-half 
times that. From the worker’s gross pay, 6% is withheld for Social Security tax, 
14% is withheld for federal income tax, 5% is withheld for state income tax, and 
$10 per week is withheld for union dues. If the worker has three or more 
dependents, then an a­dditional $35 is withheld to cover the extra cost of health 
insurance beyond what the employer pays. Write a program that will read in the 
number of hours worked in a week and the number of dependents as input and that 
will then output the worker’s gross pay, each withholding amount, and the net 
take-home pay for the week.
*/

#include <iostream>
using namespace std;

int main()
{
    const double RATE = 16.78;
    const double OVERTIME = RATE * 1.5;
    const double SOCIAL_SECURITY_TAX = 0.06;
    const double FEDERAL_INCOME_TAX = 0.14;
    const double STATE_INCOMING_TAX = 0.05;
    const double UNION_FEE = 10;
    const double FAMILY_TAX = 35;

    int n_hours = 0;
    int n_dependents = 0;

    double gross_pay = 0.0;
    double net_pay = 0.0;

    cout << "Enter number of worked hours: ";
    cin >> n_hours;

    cout << "Enter number of dependents: ";
    cin >> n_dependents;

    gross_pay = RATE * n_hours;

    double overtime_pay = 0.0;
    if (n_hours > 45) {
        // compute overtime
        overtime_pay = OVERTIME * (n_hours - 45);
    }

    double ss_tax = 0.0;
    double fed_tax = 0.0;
    double st_tax = 0.0;

    ss_tax = gross_pay * SOCIAL_SECURITY_TAX;
    fed_tax = gross_pay * FEDERAL_INCOME_TAX;
    st_tax = gross_pay * STATE_INCOMING_TAX;

    net_pay = gross_pay - ss_tax - fed_tax - st_tax - UNION_FEE + overtime_pay;

    if (n_dependents > 3) {
        net_pay -= FAMILY_TAX;
    }

    // display results:
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    double no_family = 0.0;

    cout << "gross pay:           $" << gross_pay << endl;
    cout << "overtime pay:        $" << overtime_pay << endl;
    cout << "state tax:           $" << st_tax << endl;
    cout << "social security tax: $"  << ss_tax << endl;
    cout << "federal income tax:  $" << fed_tax << endl;
    cout << "union fee:           $" << UNION_FEE << endl;

    if (n_dependents > 3) {
        cout << "family tax:          $" << FAMILY_TAX << endl;
    }
    else {
        cout << "family tax           $" << no_family << endl;
    }

    cout << "net pay:             $" << net_pay << endl;

    return 0;
}