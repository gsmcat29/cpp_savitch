// the and operator &&
#include <iostream>
using namespace std;

int main()
{
    int score = 0;

    cout << "Type a score value: ";
    cin >> score;

    if ((score > 0) && (score < 10))
        cout << "score is between 0 and 10\n";
    else
        cout << "score is not between 0 and 10\n";
    

    return 0;
}