/*
Write a program that calculates the total grade for N classroom exercises as a 
percentage. The user should input the value for N followed by each of the N 
scores and totals. 
Calculate the overall percentage (sum of the total points earned divided
by the total points possible) and output it as a percentage. Sample input and 
output are shown as follows:

How many exercises to input? 3
Score received for exercise 1: 10
Total points possible for exercise 1: 10
Score received for exercise 2: 7
Total points possible for exercise 2: 12
Score received for exercise 3: 5
Total points possible for exercise 3: 8
Your total is 22 out of 30, or 73.33%.

*/

#include <iostream>
using namespace std;

int main()
{
    double percentage = 0.0;

    int n_score = 0;
    int n_exercises = 0;

    int max_score = 0;
    int total_score = 0;
    int max_points = 0;

    cout << "How many exercises to input? ";
    cin >> n_exercises;

    for (int i = 0; i < n_exercises; ++i) {
        cout << "Score received for exercise " << i+1 << ":\t";
        cin >> total_score;
        cout << "Total points possible for exercise " << i+1 << ":\t";
        cin >> n_score;

        max_score += n_score;
        max_points += total_score;
    }
    
    // compute percentage
    percentage = static_cast<double> ( max_points) / max_score;
    percentage *= 100.0;
    cout << "Your total is " << max_points << " out of " << max_score
         << ", or " << percentage << "%.\n";

    return 0;
}